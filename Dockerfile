FROM node:alpine
WORKDIR /usr/app
# COPY package.json .
COPY . .
RUN apk --no-cache add --virtual native-deps \
  g++ gcc libgcc libstdc++ linux-headers make python && \
  npm install --quiet -g node-gyp nodemon && \
  npm install && \
  apk del native-deps
  
# CMD ["nodemon", "--legacy-watch", "server.js"]
CMD ["nodemon", "--legacy-watch", "server.js"]
EXPOSE 5000

# FROM node:latest
# RUN mkdir -p /usr/src/app
# WORKDIR /usr/src/app
# COPY package.json /usr/src/app/
# RUN npm install
# COPY . /usr/src/app
# EXPOSE 3000
# CMD [ "npm", "start" ]