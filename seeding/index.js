const {Account, Payment, PaymentTag, User} = require('../models/index')
var accountId, paymentTagId1, paymentTagId2

async function seedAccounts (userId) {
  // console.log(`Seeding accounts for ${userId}`)
  if (!userId) throw new Error(`Invalid UserId: ${userId}`)
  var account = new Account({
    title: 'test account 1',
    description: 'This is test account n.1',
    owners: [userId]
  })

  account = await account.save()
  // console.log('Account 1 saved')
  if (!account) throw account
  accountId = account._id

  var account2 = new Account({
    title: 'test account 2',
    description: 'This is test account n.2',
    owners: [userId]
  })

  account2 = await account2.save()
  // console.log('Account 2 saved')
  if (!account2) throw account2
}

async function seedPaymentTags (userId) {
  // console.log(`Seeding payment tags for user ${userId}`)
  if (!userId) throw new Error(`Invalid UserId: ${userId}`)
  var paymentTag = new PaymentTag({
    title: 'Test PT 1',
    description: 'This is a test payment tag',
    owners: [userId]
  })
  paymentTag = await paymentTag.save()
  // console.log('paymentTag 1 saved')
  if (!paymentTag) throw paymentTag
  paymentTagId1 = paymentTag._id

  var paymentTag2 = new PaymentTag({
    title: 'Test PT 2',
    description: 'This is the second test payment tag',
    owners: [userId]
  })
  paymentTag2 = await paymentTag2.save()
  // console.log('paymentTag2 saved')
  if (!paymentTag2) throw paymentTag2
  paymentTagId2 = paymentTag2._id
}

async function seedPayments () {
  // console.log(`Seeding payments for account ${accountId}`)
  if (!accountId) throw new Error(`Invalid accountId: ${accountId}`)
  if (!paymentTagId1) throw new Error(`Invalid paymentTagId1: ${paymentTagId1}`)
  if (!paymentTagId2) throw new Error(`Invalid paymentTagId2: ${paymentTagId2}`)
  // var PaymentSchema = new Schema({
  //   title: { type: String },
  //   description: String,
  //   isIncome: Boolean,
  //   amount: { type: Number, required: true },
  //   date: { type: Date, required: true },
  //   account: { type: Schema.Types.ObjectId, ref: 'Account', required: true },
  //   tags: [{ type: Schema.Types.ObjectId, ref: 'PaymentTag' }]
  // })
  var income = new Payment({
    title: 'Test income 1',
    description: 'This is a test payment',
    isIncome: true,
    amount: 1300.50,
    date: Date.now(),
    account: accountId
  })
  income = await income.save()
  // console.log('income saved')
  if (!income) throw income
  var payment1 = new Payment({
    title: 'Test payment 1',
    description: 'This is a test payment',
    isIncome: false,
    amount: 200.30,
    date: Date.now(),
    account: accountId,
    tags: [paymentTagId1, paymentTagId2]
  })
  payment1 = await payment1.save()
  // console.log('payment1 saved')
  if (!payment1) throw payment1
  var payment2 = new Payment({
    title: 'Test payment 2',
    description: 'This is a test payment',
    isIncome: false,
    amount: 100,
    date: Date.now(),
    account: accountId,
    tags: [paymentTagId1]
  })
  payment2 = await payment2.save()
  if (!payment2) throw payment2
  // console.log('payment2 saved')
  let min = 1
  let max = 1000
  for (let i = 100; i > 0; i--) {
    let p = new Payment({
      title: `Test payment ${i}`,
      description: `This is test payment n. ${i}`,
      isIncome: Math.random() >= 0.5,
      amount: (Math.random() * (max - min) + min).toFixed(2),
      date: Date.now(),
      account: accountId,
      tags: [paymentTagId1]
    })
    p = await p.save()
    if (!p) throw p
  }
}

async function removeData () {
  // console.log('Started removing data')

  // let accountsNumber = await Account.estimatedDocumentCount()
  // console.log(`Accounts found before remove ${accountsNumber}`)
  // let paymentTagsNumber = await PaymentTag.estimatedDocumentCount()
  // console.log(`Payment tags found before remove ${paymentTagsNumber}`)
  // let paymentsNumber = await Payment.estimatedDocumentCount()
  // console.log(`Payments found before remove ${paymentsNumber}`)

  let paymentTags = await PaymentTag.find()
  // console.log(`paymentTags to remove ${paymentTags.length}`)
  paymentTags.forEach(paymentTag => paymentTag.remove())

  let accounts = await Account.find()
  // console.log(`accounts to remove ${accounts.length}`)
  accounts.forEach(account => account.remove())

  let payments = await Payment.find()
  // console.log(`payments to remove ${payments.length}`)
  payments.forEach(payment => payment.remove())
  // await Account.remove()
  // await PaymentTag.remove()
  // await Payment.remove()

  // let elia = await User.findOne({email: 'elia.lombardi@outlook.it'})
  // console.log('elia after removing', elia)

  // accountsNumber = await Account.estimatedDocumentCount()
  // console.log(`Accounts found after remove ${accountsNumber}`)
  // paymentTagsNumber = await PaymentTag.estimatedDocumentCount()
  // console.log(`Payment tags found after remove ${paymentTagsNumber}`)
  // paymentsNumber = await Payment.estimatedDocumentCount()
  // console.log(`Payments found after remove ${paymentsNumber}`)

  // console.log('Ended removing data')
}

async function seedData (elia) {
  // console.log('Started seeding data')

  // let accountsNumberx = await Account.estimatedDocumentCount()
  // // console.log(`Accounts found before seed ${accountsNumberx}`)
  // let paymentTagsNumberx = await PaymentTag.estimatedDocumentCount()
  // // console.log(`Payment tags found before seed ${paymentTagsNumberx}`)
  // let paymentsNumberx = await Payment.estimatedDocumentCount()
  // // console.log(`Payments found before seed ${paymentsNumberx}`)

  let paymentTagsNumber = await PaymentTag.estimatedDocumentCount()
  // console.log(`Payment tags found ${paymentTagsNumber}`)
  if (paymentTagsNumber === 0) {
    await seedPaymentTags(elia._id)
  }

  let accountsNumber = await Account.estimatedDocumentCount()
  // console.log(`Accounts found ${accountsNumber}`)
  if (accountsNumber === 0) {
    await seedAccounts(elia._id)
  }

  let paymentsNumber = await Payment.estimatedDocumentCount()
  // console.log(`Payments found ${paymentsNumber}`)
  if (paymentsNumber === 0) {
    await seedPayments()
  }

  // accountsNumber = await Account.estimatedDocumentCount()
  // // console.log(`Accounts found after seed ${accountsNumber}`)
  // paymentTagsNumber = await PaymentTag.estimatedDocumentCount()
  // // console.log(`Payment tags found after seed ${paymentTagsNumber}`)
  // paymentsNumber = await Payment.estimatedDocumentCount()
  // // console.log(`Payments found after seed ${paymentsNumber}`)
  // let elia2 = await User.findOne({email: 'elia.lombardi@outlook.it'})
  // console.log('elia after seeding', elia2)

  // console.log('Ended seeding data')
}

async function init () {
  await User.deleteMany()
  await Account.deleteMany()
  await Payment.deleteMany()
  await PaymentTag.deleteMany()

  await new User({
    email: 'elia.lombardi@outlook.it',
    password: 'elia'
  }).save()

  let elia = await User.findOne({email: 'elia.lombardi@outlook.it'})
  if (!elia) throw new Error('Elia user not found')

  await removeData()

  await seedData(elia)
}

module.exports = {
  init
}
