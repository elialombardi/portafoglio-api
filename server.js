var express = require('express')
var app = express()
var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var session = require('express-session')
var routes = require('./routes')
const port = 5000
// process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
// const mongoUrl = "mongodb://androidapi:password@android-api_mongo_1:27017/androidapi?authSource=test";
const dbConfig = {
  mongoUrl: 'mongodb://elia.lombardi:eliaAdmin@mongo:27017/admin',
  mongoConfig: {
    useNewUrlParser: true
  }
}

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use(session({
  secret: 'bellaDeZ10',
  resave: true,
  saveUninitialized: false
}))

// DB setup
mongoose.connect(dbConfig.mongoUrl, dbConfig.mongoConfig)
  .then(() => {
    console.log('Database connected succesfully!')
    // console.log('Starting to seed database...')
    // require('./seeding/index').init()
  })
  .catch(err => {
    console.log('err', err)
    setTimeout(() => mongoose.connect(dbConfig.mongoUrl, dbConfig.mongoConfig), 1000)
  })

// Define request response in root URL (/)
app.get('/', function (req, res) {
  res.send('Hello Worldssssss!')
})

app.use('/api', routes.api)

app.listen(port,
  () => console.log(`app listening on port ${port}!`))
