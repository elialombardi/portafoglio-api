const auth = require('../libs/auth')

function getToken (req) {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') { // Authorization: Bearer g1jipjgi1ifjioj
    // Handle token presented as a Bearer token in the Authorization header
    return req.headers.authorization.split(' ')[1]
  } else if (req.query && req.query.token) {
    // Handle token presented as URI param
    return req.query.token
  } else if (req.cookies && req.cookies.token) {
    // Handle token presented as a cookie parameter
    return req.cookies.token
  }
  // If we return null, we couldn't find a token.
  // In this case, the JWT middleware will return a 401 (unauthorized) to the client for this request
  return null
}

function verifyJwtTokenMw (req, res, next) {
  let token = getToken(req)

  auth.verifyJWTToken(token)
    .then((decodedToken) => {
      req.user = decodedToken.data
      res.append('renew', auth.getJwtToken(req.user))
      next()
    })
    .catch(err => {
      res.status(400)
        .json({message: err || 'Invalid auth token provided.'})
    })
}

module.exports = verifyJwtTokenMw
