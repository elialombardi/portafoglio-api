const express = require('express');

const middleware = require('../middleware');

const router = express.Router()

/*
  R0UTES
*/


router.all('*', middleware.auth)

router.get('/', (req, res) =>
{
  res.status(200)
    .json({
      success: true,
      data: "Super secret data!"
    })
})

module.exports = router
