const express = require('express')
// const _ = require('underscore')
const middleware = require('../../middleware')
const {Payment} = require('../../models')
const router = express.Router()

router.all('*', middleware.auth)

router.get('/', (req, res) => {
  Payment.find(req.query)
    .then(payments => {
      return res.status(200).json({
        success: true,
        data: payments
      })
    }).catch(err => {
      return res.status(500).json({
        success: false,
        message: err
      })
    })
})

router.get('/:paymentId', (req, res) => {
  Payment.findById(req.params.paymentId)
    .populate('tags')
    .then(payment => {
      return res.status(200).json({
        success: true,
        data: payment
      })
    }).catch(err => {
      return res.status(500).json({
        success: false,
        message: err
      })
    })
})

router.post('/', (req, res) => {
  const newPayment = new Payment(req.body)
    .save(err => {
      if (err) return res.status(500).send(err)
      return res.status(200).send(newPayment)
    })
})

router.delete('/:paymentId', (req, res) => {
  Payment.deleteOne({_id: req.params.paymentId})
    .then(() => {
      return res.status(200).json({
        success: true
        // data: account
      })
    }).catch(err => {
      return res.status(500).json({
        success: false,
        message: err
      })
    })
})

module.exports = router
