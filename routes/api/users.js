const express = require('express')
const middleware = require('../../middleware')
const {User} = require('../../models')
const router = express.Router()

router.get('/', middleware.auth, (req, res) => {
  User.find().then(users => {
    return res.status(200).json({
      success: true,
      data: users
    })
  }).catch(err => {
    return res.status(500).json({
      success: false,
      message: err
    })
  })
})

// router.delete('/:id', function (req, res) {
//   res.send('DELETE request to homepage')
// })

router.delete('/:id', middleware.auth, (req, res) => {
  User.deleteOne({_id: req.params.id}).then(users => {
    return res.status(200).json({
      success: true
    })
  }).catch(err => {
    return res.status(500).json({
      success: false,
      message: err
    })
  })
})

router.post('/', function (req, res) {
  if (req.body.email && req.body.password) {
    var userData = {
      email: req.body.email,
      password: req.body.password
    }
    User.create(userData, function (err, user) {
      if (err) {
        return res
          .status(500)
          .json({success: false, message: err})
      } else {
        return res
          .status(200)
          .json({
            success: true,
            data: user
          })
      }
    })
  } else {
    console.log(req.body)
    res.status(500)
      .json({success: false})
  }
})

module.exports = router
