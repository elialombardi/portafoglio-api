const express = require('express')
const middleware = require('../../middleware')
const {Account} = require('../../models')
const router = express.Router()

router.all('*', middleware.auth)

router.get('/', (req, res) => {
  Account
    .find(req.query)
    .populate('payments') // To calculate the total
    .then(accounts => {
      return res.status(200).json({
        success: true,
        data: accounts
      })
    }).catch(err => {
      return res.status(500).json({
        success: false,
        message: err
      })
    })
})

router.get('/:accountId', (req, res) => {
  Account.findById(req.params.accountId)
    .populate('payments')
    .populate('owners')
    .then(account => {
      return res.status(200).json({
        success: true,
        data: account
      })
    }).catch(err => {
      return res.status(500).json({
        success: false,
        message: err
      })
    })
})

router.delete('/:accountId', (req, res) => {
  Account.deleteOne({_id: req.params.accountId})
    .then(() => {
      return res.status(200).json({
        success: true
        // data: account
      })
    }).catch(err => {
      return res.status(500).json({
        success: false,
        message: err
      })
    })
})

router.post('/', (req, res) => {
  console.log(req.user._id)
  req.body.owners = [req.user._id]
  new Account(req.body)
    .save((err, test) => {
      console.log(`err: ${err}`)
      console.log(`test: ${test}`)
      if (err) {
        return res.status(500).send({
          success: false,
          message: err
        })
      } else {
        return res.status(200).send({
          success: true,
          data: test
        })
      }
    })
})

module.exports = router
