const express = require('express')
const db = require('../../models')
const auth = require('../../libs/auth')
// const paramCheck = require('../middlewares');

const router = express.Router()

/*
  ROUTES
*/

// router.post('*', paramCheck(['email', 'password']))

router.post('/login', (req, res) => {
  let { email, password } = req.body
  db.User.authenticate(email, password)
    .then(user => {
      // console.log("args", arguments);
      return !user ? Promise.reject(new Error('User not found.')) : Promise.resolve(user)
    })
    .then((user, err) => {
      try {
        res.status(200)
          .json({
            success: true,
            token: auth.getJwtToken(user)
          })
      } catch (e) {
        console.log(e)
      }
    })
    .catch((err1, err2) => {
      res.status(401)
        .json({
          message: "Validation failed. Given email and password aren't matching."
        })
    })
})

module.exports = router
