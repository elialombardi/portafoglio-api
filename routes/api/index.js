const api = require('express').Router()
const users = require('./users')
const auth = require('./auth')
const payments = require('./payments.js')
const accounts = require('./accounts')
const paymentTags = require('./payment-tags')

api.get('/', (req, res) => {
  res.write('inside api')
})

api.use('/users', users)
api.use('/payment-tags', paymentTags)
api.use('/payments', payments)
api.use('/accounts', accounts)
api.use('/auth', auth)

module.exports = api
