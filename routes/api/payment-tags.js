const express = require('express')
const middleware = require('../../middleware')
const {PaymentTag} = require('../../models')
const router = express.Router()

router.all('*', middleware.auth)

router.get('/', (req, res) => {
  PaymentTag.find({owner: req.body.userId})
    .populate('payments')
    .then(paymentTags => {
      return res.status(200).json({
        success: true,
        data: paymentTags
      })
    }).catch(err => {
      return res.status(500).json({
        success: false,
        message: err
      })
    })
})

router.post('/', (req, res) => {
  const newPaymentTag = new PaymentTag(req.body)
    .save(err => {
      if (err) return res.status(500).send(err)
      return res.status(200).send(newPaymentTag)
    })
})

module.exports = router
