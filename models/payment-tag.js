var mongoose = require('mongoose')
var Schema = mongoose.Schema
const {User, Payment} = require('./index')

var PaymentTagSchema = new Schema({
  title: { type: String, required: true },
  description: String,
  isPublic: { type: Boolean, default: false },
  owners: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  payments: [{ type: Schema.Types.ObjectId, ref: 'PaymentTag' }]
})

PaymentTagSchema
  .virtual('id')
  .get(() => { return this._id })

async function insertReferences (paymentTag) {
  console.log(`inserting references for pt: ${paymentTag._id}`)
  if (paymentTag.owners && paymentTag.owners.length > 0) {
    console.log(`inserting pt in users: ${paymentTag.owners}`)
    User.updateMany({ _id: { $in: paymentTag.owners } }, {
      $addToSet: { paymentTags: paymentTag._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
  if (paymentTag.payments && paymentTag.payments.length > 0) {
    Payment.updateMany({ _id: { $in: paymentTag.payments } }, {
      $addToSet: { paymentTags: paymentTag._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
}

PaymentTagSchema.post('save', insertReferences)

function removeReferences () {
  console.log(`removing references for pt ${this._id}`)
  if (this.owners && this.owners.length > 0) {
    console.log(`removing pt reference from users: ${this.owners}`)
    User.updateMany({ _id: { $in: this.owners } }, {
      $pull: { paymentTags: this._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
  if (this.payments && this.payments.length > 0) {
    Payment.updateMany({ _id: { $in: this.payments } }, {
      $pull: { paymentTags: this._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
}

PaymentTagSchema.pre('remove', removeReferences)

var PaymentTag = mongoose.model('PaymentTag', PaymentTagSchema)
module.exports = PaymentTag
