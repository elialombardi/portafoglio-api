var mongoose = require('mongoose')
var bcrypt = require('bcryptjs')
var {PaymentTag, Account} = require('./index')
var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true
  },
  accounts: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Account'
  }],
  paymentTags: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'PaymentTag'
  }]
})

UserSchema
  .virtual('id')
  .get(() => { return this._id })

UserSchema.pre('save', function (next) {
  var user = this
  bcrypt.hash(user.password, 10, function (err, hash) {
    if (err) {
      return next(err)
    }
    user.password = hash
    next()
  })
})

UserSchema.statics.authenticate = function (email, password, callback) {
  let thisInstance = this
  return new Promise(function (resolve, reject) {
    thisInstance.findByEmail(email)
      .then(function (user) {
        if (!user) {
          var err = new Error('User not found.')
          err.status = 401
          reject(err)
          return
        }
        bcrypt.compare(password, user.password)
          .then(res => {
            if (res === true) {
              resolve(user)
            } else {
              var err = new Error('Wrong password.')
              err.status = 401
              reject(err)
            }
          })
      })
  })
}

UserSchema.statics.findByEmail = function (email, callback) {
  return User.findOne({ email: email }).exec()
}

async function removeReferences () {
  if (this.accounts && this.accounts.length > 0) {
    Account.updateMany({ _id: { $in: this.accounts } }, {
      $pull: { users: this._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
  if (this.paymentTags && this.paymentTags.length > 0) {
    PaymentTag.updateMany({ _id: { $in: this.paymentTags } }, {
      $pull: { users: this._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
}

UserSchema.pre('remove', removeReferences)

var User = mongoose.model('User', UserSchema)

module.exports = User
