const mongoose = require('mongoose')
const Schema = mongoose.Schema
const PaymentTag = require('./payment-tag') // se preso da index risulta undefined
const Account = require('./account')

const PaymentSchema = new Schema({
  title: { type: String },
  description: String,
  isIncome: Boolean,
  amount: { type: Number, required: true },
  date: { type: Date, required: true },
  account: { type: Schema.Types.ObjectId, ref: 'Account', required: true },
  tags: [{ type: Schema.Types.ObjectId, ref: 'PaymentTag' }]
})

PaymentSchema.path('amount').get((num) => {
  return (num / 100).toFixed(2)
})

PaymentSchema.path('amount').set((num) => {
  return num * 100
})

PaymentSchema.set('toJSON', { getters: true })

async function insertReferences (payment) {
  if (payment.tags && payment.tags.length > 0) {
    PaymentTag.updateMany({ _id: { $in: payment.tags } }, {
      $addToSet: { payments: payment._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }

  Account.updateOne({ _id: payment.account }, {
    $addToSet: { payments: payment._id }
  }, (err) => {
    if (err) throw Error(err)
  })
}

PaymentSchema.post('save', insertReferences)

async function removeReferences () {
  console.log(`removing references for payment ${this._id}`)
  if (this.tags && this.tags.length > 0) {
    console.log('PaymentTag', PaymentTag)
    PaymentTag.updateMany({ _id: { $in: this.tags } }, {
      $pull: { payments: this._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }

  Account.updateOne({ _id: this.account }, {
    $pull: { payments: this._id }
  }, (err) => {
    if (err) throw Error(err)
  })
}

PaymentSchema.pre('remove', removeReferences)

PaymentSchema
  .virtual('id')
  .get(() => { return this._id })

var Payment = mongoose.model('Payment', PaymentSchema)
module.exports = Payment
