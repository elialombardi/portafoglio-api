var mongoose = require('mongoose')
var Schema = mongoose.Schema
const {User, Payment} = require('./index')

var accountSchema = new Schema({
  title: { type: String, required: true },
  description: String,
  owners: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
    require: true
  }],
  payments: [{
    type: Schema.Types.ObjectId,
    ref: 'Payment'
  }]
})

accountSchema.virtual('total').get(function () {
  if (this.payments.length === 0) {
    return parseFloat(0).toFixed(2)
  } else {
    return parseFloat(this.payments
      // .map(el => parseFloat(el.amount))
      // .reduce((memo, p) => parseFloat(p) + parseFloat(memo)))
      .reduce((memo, p, i) => {
        if (i === 1) memo = parseFloat(0)
        return p.isIncome === true ? parseFloat(memo) + parseFloat(p.amount) : parseFloat(memo) - parseFloat(p.amount)
      }))
      .toFixed(2)
  }
})
accountSchema.set('toJSON', { virtuals: true })

accountSchema
  .virtual('id')
  .get(() => { return this._id })

async function insertReferences (account) {
  if (account.owners && account.owners.length > 0) {
    User.updateMany({ _id: { $in: account.owners } }, {
      $addToSet: { accounts: account._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
  if (account.payments && account.payments.length > 0) {
    Payment.updateMany({ _id: { $in: account.payments } }, {
      $addToSet: { accounts: account._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
}

accountSchema.post('save', insertReferences)

async function removeReferences () {
  if (this.owners && this.owners.length > 0) {
    User.updateMany({ _id: { $in: this.owners } }, {
      $pull: { accounts: this._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
  if (this.payments && this.payments.length > 0) {
    Payment.updateMany({ _id: { $in: this.payments } }, {
      $pull: { accounts: this._id }
    }, (err) => {
      if (err) throw Error(err)
    })
  }
}

accountSchema.pre('remove', removeReferences)

var Account = mongoose.model('Account', accountSchema)
module.exports = Account
